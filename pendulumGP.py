"""

step 1: fit a inverted pendulum model using GPy
step 2: predict new points using fit GP model
step 3: update GP model using new datapoints
step 4: sample models from GP distribution
step 5: sample a trajectory according to sampled dynamics

"""

import os.path

import numpy as np
import pytest
from casadi import MX, Function
from casadi import reshape as cas_reshape

from safe_exploration.ssm_gpy.gaussian_process import SimpleGPModel

try:
    from safe_exploration.ssm_gpy.gaussian_process import SimpleGPModel
    from GPy.kern import RBF
    _has_ssm_gpy = True
except:

    _has_ssm_gpy = False

import safe_exploration.gp_reachability as reach_num
import safe_exploration.gp_reachability_casadi as reach_cas
from safe_exploration.utils import array_of_vec_to_array_of_mat

import copy

# step 1: fit GP model (pendulum)
env, init_uncertainty, lin_model = "InvPend", True, True
n_s = 2
n_u = 1
c_safety = 2
a = None
b = None 
if lin_model:
    a = np.random.rand(n_s, n_s)
    b = np.random.rand(n_s, n_u)

train_data = np.load(os.path.join(os.path.dirname(__file__), 'invpend_data.npz'))
X = train_data['X']
y = train_data["y"]
m = 50

gp = SimpleGPModel(n_s, n_s, n_u, X, y, m, train=False)
gp.train(X, y, m, opt_hyp=True, choose_data=False)
L_mu = np.array([0.001] * n_s)
L_sigm = np.array([0.001] * n_s)

# step 2: predict new points using fit GP model
index = np.random.choice(range(10), 1)
testX = train_data['X'][index, ]
testY = train_data['y'][index, ]
# First dimension is 1*2 state
# second dimension is 1*1 action
mu, sigma, _ = gp(testX[:,:2], testX[:,2])

# step 3: sample n_samples trajectory using GP
# n_samples: number of samples
n_samples = 200
# n_steps: number of steps
n_steps = 5

# Given a randomly initialized state
x = np.random.normal(size=2).reshape([1,-1])
# We first construct n_steps control actions
actions = np.random.normal(loc=0.0, scale=1.0, size=[n_steps])
# Also a feedback control law K
K = np.random.normal(size=[2])
# Then we iterate n_steps
trajectories = []

# gp_dict = copy.copy(gp.to_dict())

import time
times = []
for i in range(n_samples):
    steps = []
    gp_copy = copy.copy(gp)
    # gp_copy = SimpleGPModel(n_s, n_s, n_u)
    # gp_copy.from_dict(gp_dict)
    start = time.time()
    for j in range(n_steps):
        # u = (x.dot(K) + actions[j]).reshape([1,-1])
        action = actions[j].reshape([1,-1])
        mu, sigma, _ = gp_copy(x, action)
        trainX = np.hstack([x, action])
        mu = np.array(mu)
        sigma = np.array(sigma)
        ep = np.random.normal(size=[2,1])
        x = (mu + ep * sigma).reshape([1,-1])
        trainY = np.array(x, copy=True)
        gp_copy.update_model(trainX, trainY)
        steps.append(x)
    trajectories.append(steps)
    end = time.time()
    times.append([end-start])

import matplotlib.pyplot as plt
mean = np.mean(times)
total_time = np.sum(times)
print('mean',mean)
print('total time', total_time)
plt.plot(range(200), times)
plt.show()

# step 4: Update GP using newly sampled data points
# First, we generate some sample points
N_SAMP = 100 # number of sample points
samplesX = np.random.normal(loc=0.0, scale=1.0, size=[N_SAMP, 3])
samplesY = np.random.normal(loc=0.0, scale=1.0, size=[N_SAMP, 2])
# update GP model using sampled points
gp.update_model(samplesX, samplesY)